import sqlite3
from pathlib import Path


class model:
    # pylint: disable=too-many-instance-attributes
    def __init__(
        self,
        fname,
        file_size,
        title,
        author,
        created,
        modified,
        textPdf,
        contents,
        file_id,
    ):
        self.fname = fname
        self.file_size = file_size
        self.title = title
        self.author = author
        self.created = created
        self.modified = modified
        self.textPdf = textPdf
        self.contents = contents
        self.file_id = file_id
        self._con = sqlite3.connect(str(Path.home()) + "/pdfextractor/pdfextractor/pdfextractor.db")

    # Insert Pdf Information to sqlite database
    def insert_pdf(self):
        with self._con as cursor:
            cursor.execute(
                "INSERT INTO pdf_details VALUES ("
                "'"
                + str(self.fname)
                + "','"
                + str(self.file_size)
                + "','"
                + str(self.title)
                + "','"
                + str(self.author)
                + "','"
                + str(self.created)
                + "','"
                + str(self.modified)
                + "','"
                + str(self.textPdf)
                + "','"
                + str(self.contents)
                + "');"
            )
            cursor.commit()
            pdf_id = cursor.execute("select max(rowid) from pdf_details;")
        for i in pdf_id:
            return str(i[0])

    # Select query to get the metadata of the PDF
    def get_metadata(self):
        for row in self._con.execute("""select rowid,doc_name,doc_size, title,author,
                createddate,modifieddate from pdf_details where rowid= '"""
            + str(self.file_id)
            + "';"
        ):
            return row

    # Select query to display text of a specific id
    def get_text(self):
        for row in self._con.execute(
            "select doc_text from pdf_details where rowid= '" + str(self.file_id) + "';"
        ):
            return row

    # Select query to display PDF of a specific id
    def get_pdf(self):
        for row in self._con.execute(
            "select pdf_file from pdf_details where rowid= '" + str(self.file_id) + "';"
        ):
            return row

    # Select query to get PDF file name
    def get_doc_name(self):
        for row in self._con.execute(
            "select doc_name from pdf_details where rowid= '" + str(self.file_id) + "';"
        ):
            return row
