import base64
import datetime
import os
import re

import pdfminer.high_level
from flask import render_template, request, send_from_directory
from PyPDF2 import PdfFileReader
from pdfextractor.model.model import model


class Controller:
    def __init__(self):
        pass

    # function to upload the file
    @staticmethod
    def upload_file():
        return render_template("upload.html")

    # pdf Check validity function
    @staticmethod
    def check_validity(content_type):
        if str(content_type) not in "application/pdf":
            return "False"
        return"True"

    # extract metadata from pdf file and insert them in the database
    @staticmethod
    def upload_file_t():
        if request.method == "POST":
            pdf_file = request.files["file"]
            if Controller.check_validity(pdf_file.headers["content-type"]) == "False":
                return render_template("invalidPDF.html")
            metadata = pdf_file.headers["content-disposition"]
            fname = re.findall("filename=(.+)", metadata)[0]
            request.files["file"].save("PDF/" + str(fname))
            path = "PDF/" + str(fname)

            # Change Size Format
            file_size = str((os.path.getsize(path)) / 1000) + " KB"
            pdf = PdfFileReader(pdf_file)
            information = pdf.getDocumentInfo()

            # convert the file to base64
            with open("PDF/" + str(fname), "rb") as binary_file:
                binary_file_data = binary_file.read()
                base64_encoded_data = base64.b64encode(binary_file_data)
                base64_message = base64_encoded_data.decode()
                contents = base64_message

            # convert the pdf to text
            with open("PDF/" + str(fname), "rb") as pdf_file:
                text_pdf = pdfminer.high_level.extract_text(pdf_file)

			# Change Date format
            if "CreationDate" in str(pdf.documentInfo):
                created = datetime.datetime.strptime(
                        pdf.documentInfo["/CreationDate"][0:15], "D:%Y%m%d%H%M%S"
                        ).strftime("%Y-%m-%d %H:%M:%S")
            else:
                created = None

            if "ModDate" in str(pdf.documentInfo):
                modified = datetime.datetime.strptime(
                        pdf.documentInfo["/ModDate"][0:15], "D:%Y%m%d%H%M%S"
                        ).strftime("%Y-%m-%d %H:%M:%S")
            else:
                modified = None

            # insert the fields to the database
            model_obj = model(
                fname,
                file_size,
                information.title,
                information.author,
                created,
                modified,
                text_pdf,
                contents,
                "",
            )
            value = model_obj.insert_pdf()
            return render_template("metadata.html", rows=value)
        return "get Method"

    # list the metadata
    @staticmethod
    def list(file_id):
        model_obj = model("", "", "", "", "", "", "", "", file_id)
        rows = model_obj.get_metadata()
        if rows is None:
            return "The current id does not exist please choose another one"
        return render_template("list.html", rows=rows)

    # display text
    @staticmethod
    def text(file_id):
        model_obj = model("", "", "", "", "", "", "", "", file_id)
        row = model_obj.get_text()
        if row is None:
            return render_template("invalidID.html")
        return str(row)

    # display pdf
    @staticmethod
    def pdf(file_id):
        model_obj = model("", "", "", "", "", "", "", "", file_id)
        row = model_obj.get_pdf()
        if row is None:
            return render_template("invalidID.html")
        base64_message = row[0]
        base64_bytes = base64_message.encode()
        with open("getPDF/"+str(file_id) + ".pdf", "wb") as pdf_file:
            decoded_image_data = base64.decodebytes(base64_bytes)
            pdf_file.write(decoded_image_data)
            pdf_file.close()
            workingdir = os.path.abspath(os.getcwd())
            return send_from_directory(workingdir + "/getPDF", str(file_id) + ".pdf")
