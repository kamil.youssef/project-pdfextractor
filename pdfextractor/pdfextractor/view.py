#from flask import Flask
from pdfextractor.controller.controller import Controller

from pdfextractor import app


# upload pdf file
@app.route("/upload")
def upload_file():
    return Controller.upload_file()

# save pdf file with metadata
@app.route("/uploader", methods=["GET", "POST"])
def upload_file_t():
    return Controller.upload_file_t()

# view metadata of a pdf file
@app.route("/metadata/<file_id>")
def list_metadata(file_id):
    return Controller.list(file_id)

# view text output of a pdf file
@app.route("/text/<file_id>")
def text(file_id):
    return Controller.text(file_id)

# review the pdf file inserted
@app.route("/pdf/<file_id>")
def pdf(file_id):
    return Controller.pdf(file_id)

if __name__ == "__main__":
    app.run(debug=True)
