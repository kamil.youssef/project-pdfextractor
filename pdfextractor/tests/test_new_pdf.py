from pdfextractor.model.model import model

def test_inserted_data():
    pdf = model("Variational Probabilistic Multi-Hypothesis Tracking"
            ,""
            ,"","Shuoyuan Xu, Hyo-Sang Shin and Antonios Tsourdos;"
            , "","23","","","")
    assert pdf.fname == "Variational Probabilistic Multi-Hypothesis Tracking"
    assert pdf.author == "Shuoyuan Xu, Hyo-Sang Shin and Antonios Tsourdos;"
