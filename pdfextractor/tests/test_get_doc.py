from pdfextractor.controller.controller import Controller

def test_id_validity():
    print(Controller.list("10000000000000000"))
    assert Controller.list("10000000000000000") == "The current "\
            "id does not exist please choose another one"
