from datetime import date
import pytest
from pdfextractor.model.model import model
from pdfextractor.view import app

@pytest.fixture
def insert_pdf():
    pdfobj=model("1","Hello","13KB","world","Jhon",date.today(),date.today(),"","")
    docid=pdfobj.insert_pdf()
    print("docid " + str(docid))
    return docid


def test_router(insert_pdf):
    with app.test_client() as test_client:
        response = test_client.get("/upload")
        assert response.status_code == 200

        response = test_client.get("/uploader")
        assert response.status_code == 200

        response = test_client.get("/metadata/" + str(insert_pdf) )
        assert response.status_code == 200

        response = test_client.get("/text/" + str(insert_pdf))
        assert response.status_code == 200

        response = test_client.get("/pwdwdw")
        assert response.status_code == 404

        response = test_client.get("/pdf/" + str(insert_pdf) + ".txt")
        assert response.status_code == 200
