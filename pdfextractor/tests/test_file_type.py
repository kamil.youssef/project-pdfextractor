from pdfextractor.controller.controller import Controller
import magic

def test_pdf_type():
    mime = magic.Magic(mime=True)
    content_type=mime.from_file("21234543.pdf")
    assert Controller.check_validity(content_type)=="True"
    content_type=mime.from_file("export.txt")
    assert Controller.check_validity(content_type)=="False"
