# PDF Extractor Application



## Install Prerequisites:



### install SQLite, python, pip:

```bash
apt install python3.9 sqlite3 python3-pip python3-flask pylint python3-pytest
```



### install python packages:

Install python packages using the following command:

```bash
cd pdfextractor/
pip install -r requirements.txt
```



## Create Table in SQLite



Table already created incase it did not work here are the steps that you can follow to run the application:

```bash
sqlite3 pdfextractor.db
```

```sqlite
CREATE TABLE pdf_details(doc_name varchar,doc_size varchar, title varchar,author varchar, createddate timestamp,modifieddate timestamp, doc_text CLOB, pdf_file CLOB);
```



## Extract the App and run it:



Run the app in the home folder:

```bash
cd pdfextractor/pdfextractor/
FLASK_APP=view.py flask run
```

Here are the links to view the application:

- To upload a PDF: http://localhost:5000/upload 
- To view the Metadata: http://localhost:5000/metadata/id
- To display the text of a specific pdf: http://localhost:5000/text/id
- to view the PDF inserted: http://localhost:5000/pdf/id

## Pylint

run the following command in the directory pdfextractor/pdfextractor:

```bash
cd pdfextractor/pdfextractor
pylint .
pylint view.py
pylint controller/controller.py
pylint model/model.py
```



## TEST

run the following commands:

```bash
cd pdfextractor/tests
pytest-3
python3.9 -m pytest --cov=../pdfextractor/ --cov-report html
```

you can see the report tests covering 73%

